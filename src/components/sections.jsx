import React from "react";
import WikiImg from "../media/backgrounds/wiki.jpg";
import GalleryImg from "../media/backgrounds/gallery.jpg";
import WikiTitle from "../media/titles/wiki-title.png";
import GalleryTitle from "../media/titles/gallery-title.png";

const Sections = ({ data }) => {
  return (
    <React.StrictMode>
      {data.map((section) => {
        if (section.imgSrc)
          return (
            <div className="row justify-content-center no-gutters">
              <div className="col-lg-6">
                <img
                  className="img-fluid"
                  src={section.imgSrc === "wiki" ? WikiImg : GalleryImg}
                  alt={section.imgAlt}
                />
              </div>
              <div className="col-lg-6">
                <div className="bg-black text-center h-100 project">
                  <div className="d-flex h-100">
                    <div className="project-text w-100 my-auto text-center text-lg-left">
                      <img
                        className="img-fluid mb-3 mb-lg-0"
                        src={
                          section.titleSrc === "wiki-title"
                            ? WikiTitle
                            : GalleryTitle
                        }
                        alt={section.titleAlt}
                      />
                      <p className="mb-0 text-white-50">{section.desc}</p>
                      <hr className="d-none d-lg-block mb-0 ml-0" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        return;
      })}
    </React.StrictMode>
  );
};

export default Sections;
