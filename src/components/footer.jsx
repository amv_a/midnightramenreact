import React from "react";
import Signup from "./signup";
import Contact from "./commons/contact";

const Footer = (props) => {
  return (
    <React.StrictMode>
      <Signup />
      <Contact />
      <footer className="bg-black small text-center text-white-50">
        <div className="container">
          <p>Las imagenes que aparecen en esta web son creative commons.</p>
          Copyright &copy; Midnight Ramen 2019
        </div>
      </footer>
    </React.StrictMode>
  );
};

export default Footer;
