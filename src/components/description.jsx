import React from "react";

const Description = (props) => {
  return (
    <section id="intro" className="about-section text-center">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 mx-auto">
            <h2 className="text-white mb-4">
              <img
                id="imgLogo"
                className="mx-auto d-block img-fluid"
                src={require("../media/titles/description-title.png")}
                alt="Midnight Ramen Logo"
              />
            </h2>
            <p className="text-white-50">
              Es un juego text-based y de rol por turnos con un universo de
              ciencia ficción que tiene lugar en una ciudad enorme
              semi-futurista donde la gente diferente es invsible a ojos de la
              sociedad, sigue la historia del protagonista y decide su camino.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Description;
