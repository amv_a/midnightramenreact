import React, { Component } from "react";
import getSocialNetworks from "../../services/fakeSocialService";

class Contact extends Component {
  state = {
    socialNetWorks: [],
  };

  componentDidMount() {
    this.setState({ socialNetWorks: getSocialNetworks() });
  }
  render() {
    return (
      <section className="contact-section bg-black">
        <div className="container">
          <div className="social d-flex justify-content-center">
            {this.state.socialNetWorks.map((network) => (
              <a href={network.ref} className="mx-2">
                <i className={network.icon}></i>
              </a>
            ))}
          </div>
        </div>
      </section>
    );
  }
}

export default Contact;
