import React from "react";

const Header = () => {
  return (
    <header className="masthead" id="masthead">
      <div className="container d-flex h-100 align-items-center">
        <div className="mx-auto text-center">
          <a id="linkLogo" href="index.html">
            <img
              id="imgLogo"
              className="mx-auto d-block img-fluid"
              src="logo.png"
              alt="Midnight Ramen Logo"
            />
          </a>
          <a
            id="play"
            href="#about"
            className="btn btn-primary js-scroll-trigger"
          >
            Juega Ahora
          </a>
        </div>
      </div>
    </header>
  );
};

export default Header;
