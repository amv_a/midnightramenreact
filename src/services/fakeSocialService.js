export const socialNetworks = [
  { _id: 1, ref: "#", icon: "fab fa-twitter" },
  { _id: 2, ref: "#", icon: "fab fa-youtube" },
  { _id: 3, ref: "#", icon: "fab fa-github" },
  { _id: 4, ref: "#", icon: "fas fa-envelope" },
  { _id: 5, ref: "#", icon: "fa fa-twitch" },
  {
    _id: 6,
    ref: "https://www.youtube.com/watch?v=-bzWSJG93P8",
    icon: "fa fa-ge",
  },
];

export default function getSocialNetworks() {
  return socialNetworks.filter((s) => s);
}
