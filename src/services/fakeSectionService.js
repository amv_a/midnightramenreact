export const sections = [
  { _id: "mainPage", name: "Página Principal" },
  {
    _id: "wiki",
    name: "Glosario",
    imgSrc: "wiki",
    imgAlt: "Una foto para la descripción del glosario",
    titleSrc: "wiki-title",
    titleAlt: "Titulo: Glosario",
    desc:
      "Descubre mas sobre el juego y su mundo, sus personajes, la sociedad" +
      " y acontecimientos.",
  },
  {
    _id: "gallery",
    name: "Galería",
    imgSrc: "gallery",
    imgAlt: "Una foto para la descripción de la galería",
    titleSrc: "gallery-title",
    titleAlt: "Titulo: Galería",
    desc:
      "Aquí podrás encontrar los lugares en los que tiene lugar la " +
      "historia y imágenes de los personajes y enemigos del juego.",
  },
  { _id: "chgLog", name: "Versiones" },
  { _id: "toCome", name: "Futuras Actualizaciones" },
  { _id: "bugs", name: "Sugerencias" },
];

export default function getSections() {
  return sections.filter((s) => s);
}
