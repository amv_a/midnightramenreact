import React from "react";
import Sections from "./sections";

const MainContent = (props) => {
  return (
    <section id="projects" className="projects-section bg-light">
      <div className="container">
        <div className="row align-items-center no-gutters mb-4 mb-lg-5">
          <div className="col-xl-8 col-lg-7">
            <img
              className="img-fluid mb-3 mb-lg-0"
              src={require("../media/backgrounds/game.jpg")}
              alt="Una foto para la descripcion del juego"
            />
          </div>
          <div className="col-xl-4 col-lg-5">
            <div className="featured-text text-center text-lg-left">
              <img
                className="img-fluid mb-3 mb-lg-0"
                src="media/titles/game-title.png"
                alt="Titulo: El juego"
              />
              <p className="text-black-50 mb-0">
                Enfrentate a una socidedad decadente y ciega, mas centrada en
                las apariencias y la extravagancia que en la vida de las
                personas.
              </p>
            </div>
          </div>
        </div>
        <Sections data={props.sections} />
      </div>
    </section>
  );
};

export default MainContent;
