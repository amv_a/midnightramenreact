import React, { Component } from "react";
import jQuery from "jquery";
import NavBar from "./components/navBar";
import Header from "./components/header";
import Description from "./components/description";
import MainContent from "./components/mainContent";
import Footer from "./components/footer";
import getSections from "./services/fakeSectionService";
import "./midnightRamenNight.min.css";

class App extends Component {
  state = {
    sections: [],
  };

  componentDidMount() {
    this.setState({ sections: getSections() });
    (function ($) {
      "use strict"; // Start of use strict

      // Closes responsive menu when a scroll trigger link is clicked
      $(".js-scroll-trigger").click(function () {
        $(".navbar-collapse").collapse("hide");
      });

      // Collapse Navbar
      var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
          $("#mainNav").addClass("navbar-shrink");
        } else {
          $("#mainNav").removeClass("navbar-shrink");
        }
      };
      // Collapse now if page is not at top
      navbarCollapse();
      // Collapse the navbar when page is scrolled
      $(window).scroll(navbarCollapse);
    })(jQuery); // End of use strict
  }
  render() {
    return (
      <React.StrictMode>
        <NavBar sections={this.state.sections} />
        <Header />
        <Description />
        <MainContent sections={this.state.sections} />
        <Footer />
      </React.StrictMode>
    );
  }
}
export default App;
